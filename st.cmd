#!/usr/bin/env iocsh.bash

require(s7plc)
require(modbus)
require(calc)

epicsEnvSet("TOP", "../cwm-cws03_ctrl-plc-01")
cd $(TOP)

iocshLoad("cwm_cws03_ctrl_plc_01.iocsh", "IPADDR=172.30.5.218, RECVTIMEOUT=2000, cwm_cws03_ctrl_plc_01_VERSION=plcfactory")
